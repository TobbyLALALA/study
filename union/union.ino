/*
 * union学习用代码
 * 感谢︶ㄣ第二名、奈何col、BeyondTechnology、byfei
 * 2019.7.29验证成功
 */

union test                //定义test联合体
{  
  int office;             //整型成员office
  byte teacher[2];        //字节数组成员teacher
}a,b;                     //定义同时说明a，b为test类型
                          //a,b变量的长度应等于test的成员中最长的长度，
                          //即等于teacher数组的长度，共2个字节。
void setup() 
{
 
  a.office = 0xff00;      //即1111 1111 0000 0000
  Serial.begin(9600);
}

void loop() 
{
  Serial.println(a.teacher[0]);//提取前8位：0000 0000
  Serial.println(a.teacher[1]);//提取后8位：1111 1111
}
