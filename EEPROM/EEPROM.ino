/*
 * 参考于shenhaiyu发表于极客工坊、卢光庆发表于CSDN、奈何col发表于Arduino中文社区的代码
 * 用于将大于八位的变量以共用体的形式存入EEPROM
 * 验证成功
 * 2019.7.29
 */

#include <EEPROM.h>

union data 
{
  int a;
  byte b[2];
} distance;

void setup() 
{
  Serial.begin(9600);

  //清空数据（每个地址都写0）
  for (int i = 0; i < 512; i++)
  {
     EEPROM.write(i, 0);  
  }
  
  //数据拆分
  distance.a = 32000;
  for(int i = 0; i < 2; i++) 
  {
    EEPROM.write(i,distance.b[i]);
  }

}

void loop()
{
  //数据还原
  for(int i = 0; i < 2; i++)
  {
    distance.b[i] = EEPROM.read(i);
  }
  Serial.println(distance.a);
  delay(1000);
}
